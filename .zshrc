ZSH_THEME="bira"
plugins=(
  git vi-mode z per-directory-history screen gitignore
)
source $ZSH/oh-my-zsh.sh


###################
# User Configuration

# editor
export VISUAL=vim
export EDITOR="$VISUAL"

# vi-mode plugin
bindkey -M viins 'jk' vi-cmd-mode

# aliases
alias artisan="ea-php73 artisan"
alias tinker="ea-php73 artisan tinker"
alias composer="ea-php73 -d allow_url_fopen=On -d memory_limit=1G ~/bin/composer"
alias ll="ls -lAFh" # for bash users

# Hello messages
if [ -f ~/.rci/welcome.rb ]; then
  ruby ~/.rci/welcome.rb
fi



