
 " Ix Vim customizations
 inoremap jk <Esc>
 nmap <Leader><Space> :nohlsearch<CR>
 set number

 " vim-go needs newer version that RCI production, but don't need so ignore
 let g:go_version_warning = 0

