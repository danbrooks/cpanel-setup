#!/bin/sh

clear

echo "####################"
echo "Running cpanel setup..."
echo 
echo "User: $USER"
echo "Home: $HOME"
echo
project_dir=`pwd`
cd $HOME


#########################################
echo "####################"
echo "Setting up Vim"
vim_repo_dir="$HOME/.vim_runtime"
vim_config_file_destination="$vim_repo_dir/my_configs.vim"
if [ -d "$vim_repo_dir" ]; then
    echo "...found previous install, skipping"
else
    git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
    sh ~/.vim_runtime/install_awesome_vimrc.sh
    cp $project_dir/my_configs.vim $vim_config_file_destination
    echo "...Vim setup complete"
fi
cd $HOME
echo


#########################################
echo "####################"
echo "Setting up Oh My Zsh"
ohmyzsh_dir="$HOME/.oh-my-zsh"
zshrc_file="$HOME/.zshrc"
bash_profile="$HOME/.bash_profile"
if [ -d "$ohmyzsh_dir" ]; then
    echo "...found previous install, skipping"
else
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    cp $project_dir/.zshrc $zshrc_file
    sed -i "1s|^|export ZSH=$ohmyzsh_dir\n|" $zshrc_file
    if [ ! -f "$bash_profile" ]; then
        cp $project_dir/.bash_profile $bash_profile
    else
        sed -i "1s|^|export SHELL=/bin/zsh\n|" $bash_profile
        sed -i "2s|^|exec /bin/zsh -l\n|" $bash_profile
    fi
    cp $project_dir/.zlogout $HOME/.zlogout
    echo "...Oh My Zsh setup complete"
fi
cd $HOME
echo


#########################################
echo "####################"
echo "Setting up Composer"
if [ -f "$HOME/bin/composer" ]; then
    echo "... Composer found, skipping"
else
    if [ ! -d "$HOME/bin" ]; then
        mkdir $HOME/bin
    fi
    cd $HOME/bin
    EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig)
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" -d allow_url_fopen=On
    ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

    if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
    then
        >&2 echo 'ERROR: Invalid Composer installer signature'
        rm composer-setup.php
    else
        php -d allow_url_fopen=On composer-setup.php --quiet
        RESULT=$?

        rm composer-setup.php
        mv composer.phar $HOME/bin/composer
        chmod 700 $HOME/bin/composer
    fi
fi
cd $HOME
echo



#########################################
echo "####################"
echo "Final settings..."
if [ ! -d "$HOME/.rci" ]; then
    echo " - adding messages"
    mkdir $HOME/.rci
    cp $project_dir/welcome.rb $HOME/.rci/welcome.rb
    cp $project_dir/welcome.txt $HOME/.rci/welcome.txt
    cp $project_dir/goodbye.rb $HOME/.rci/goodbye.rb
    cp $project_dir/goodbye.txt $HOME/.rci/goodbye.txt
fi

if [ ! -f "$HOME/.inputrc" ]; then
    echo " - custom .inputrc"
    cp $project_dir/.inputrc $HOME/.inputrc
fi
echo


#########################################
echo "####################"
echo "SETUP COMPLETE"
echo "####################"

